/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cgi_list;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author johantv
 */
public class List {
    
    ArrayList<Integer> al = new ArrayList<>();
    // reset list
    void resetList() {
        al.clear();
    }

    // return list size
    int getListSize() {
        return al.size();
    }

    // add item to list
    void addItem(int i) {
        al.add(i);
    }
    
    // return list content
    String printList() {
        return al.toString();
    }
    
    
    // calculate minimum distance of items in list
    public int getMinimum() {
        //first sort list from smallest to largest
        Collections.sort(al);
        
        //set minimum first to be largest item in list
        //  since minimum distance can be at max largest item - 0 (in case
        //  there would be just 2 items in list)
        int minimum = al.get(al.size()-1);
        //go through list
        for (int i=1; i<al.size(); i++) {
            int current = al.get(i);
            int previous = al.get(i-1);
            
            //compare distance between current and previous to minimum
            if ((current-previous-1) < minimum && current!=previous) {
                // if new distance is smaller, set new minimum
                // (minus 1 because if numbers are 6 and 3 -> minimum is 3 but
                //   there will only be 2 numbers between (4 and 5)
                minimum = current-previous-1;
            }
        }
        return minimum;
    }
}
