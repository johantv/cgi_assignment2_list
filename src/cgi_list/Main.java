/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cgi_list;

import java.util.Random;

/**
 *
 * @author johantv
 */
public class Main {
    public static void main(String[] args) {
        //create new list object
        List l = new List();
        
        //add 10 random numbers to list
        int counter = 0;
        Random random = new Random();
        while (counter < 10) {
            //since java random generator is not really that random, set range to 200 (increases probability of minimum distance being larger than 1)
            int randomInt = random.nextInt(200);
            l.addItem(randomInt);
            counter++;
        }
        
        //print minimum
        System.out.println("minimum: " + l.getMinimum());
        
        //print list
        System.out.println(l.printList());
    }
    
}
