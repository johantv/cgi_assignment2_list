/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cgi_list;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author johantv
 */
public class ListTest3 {
    private static List list;
 
    @BeforeClass
    public static void testSetUpList() {
        System.out.println("Creating list object");
        list = new List();
    }

    @BeforeClass
    public static void testClear() {
        System.out.println("Reseting list");
        list.resetList();
        assertEquals("Reseting not working", 0, list.getListSize());
    }
    
    @Test
    public void testAddNewItem() {
        System.out.println("Test adding new items part 3");
        list.addItem(10);
        list.addItem(1);
        list.addItem(4);
        assertEquals("Adding item to list not working (part 3)", "[10, 1, 4]", list.printList());
    }
    
    @Test
    public void testFindMinimum() {
        System.out.println("Test finding minimum (part 3)");
        assertEquals("Finding minimum returned wrong result", 2, list.getMinimum());
    }
}
