/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cgi_list;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author johantv
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({cgi_list.ListTest.class, cgi_list.ListTest2.class, cgi_list.ListTest3.class, cgi_list.ListTest4.class})
public class ListTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("***** Starting to run tests *****");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        System.out.println("***** Finished running tests *****");
    }
    
}
