/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cgi_list;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author johantv
 */
public class ListTest2 {
    private static List list;
 
    @BeforeClass
    public static void testSetUpList() {
        System.out.println("Creating list object");
        list = new List();
    }
    
    @BeforeClass
    public static void testClear() {
        System.out.println("Reseting list");
        list.resetList();
        assertEquals("Reseting not working", 0, list.getListSize());
    }
    
    @Test
    public void testAddNewItem() {
        System.out.println("Test adding new items part 2");
        list.addItem(3);
        list.addItem(15);
        list.addItem(8);
        list.addItem(22);
        assertEquals("Adding item to list not working (part 2)", "[3, 15, 8, 22]", list.printList());
    }
    
    @Test
    public void testFindMinimum() {
        System.out.println("Test finding minimum (part 2)");
        assertEquals("Finding minimum returned wrong result", 4, list.getMinimum());
    }
}
